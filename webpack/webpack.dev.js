const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

const outputDirectory = '../dist';

module.exports = merge(common, {
  mode: 'development',
  devtool: 'eval',
  devServer: {
    host: '0.0.0.0',
    port: 3000,
    open: true,
    hot: true,
    contentBase: path.join(__dirname, outputDirectory),
    historyApiFallback: true,
    proxy: {
      '/api': 'http://localhost:8080'
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
});
