import {
  COMPATIBILITY_CHECK_REQUEST,
  COMPATIBILITY_CHECK_FAILURE,
  COMPATIBILITY_CHECK_SUCCESS,
  MODELS_LOAD_REQUEST,
  MODELS_LOAD_FAILURE,
  MODELS_LOAD_SUCCESS,
  ANSWER_LAST,
  CHANGE_VIEW,
  CHANGE_CURRENT_QUESTION,
  ANSWER_QUESTION,
  RESET_ANSWERS,
  BEGIN_IMMERSION,
  CATCH_ERROR,
  SET_RENDERER,
  SET_VRDISPLAY,
  SET_PRESENT,
  SET_STAGE
} from './types';

import errors from '../data/errorTypes';
import questions from '../data/questions';
import stages from '../data/appStages';

import views from '../views';

import { gyroCheck, checkSuccess } from '../util/gyroCheck';

export const setStage = appStage => ({
  type: SET_STAGE,
  appStage
});

export const addAnswer = answer => ({
  type: ANSWER_QUESTION,
  answer
});

export const setQuestion = currentQuestion => ({
  type: CHANGE_CURRENT_QUESTION,
  currentQuestion
});

export const setView = view => ({
  type: CHANGE_VIEW,
  view
});

export const beginImmersion = () => ({ type: BEGIN_IMMERSION });

export const loadModelsRequest = () => ({ type: MODELS_LOAD_REQUEST });
export const loadModelsSuccess = () => ({ type: MODELS_LOAD_SUCCESS });
export const loadModelsFailure = () => ({ type: MODELS_LOAD_FAILURE });

export const setRenderer = renderer => ({
  type: SET_RENDERER,
  renderer
});

export const togglePresent = () => (dispatch, getState) => {
  const { vrDisplay, renderer, presenting } = getState().vr;

  if (vrDisplay.isPresenting) {
    vrDisplay.exitPresent();
    if (presenting) {
      dispatch({ type: SET_PRESENT, presenting: false });
    }
  } else {
    vrDisplay.requestPresent([{ source: renderer.domElement }]);
    if (!presenting) {
      dispatch({ type: SET_PRESENT, presenting: true });
    }
  }
};

export const answerLastQuestion = () => (dispatch, getState) => {
  const { flags, answers } = getState();

  if (!flags.allAnswered && Object.keys(answers).length === questions.length) {
    dispatch({ type: ANSWER_LAST });
  }

  dispatch(setView(views.CONFIRM));
};

export const catchError = error => (dispatch) => {
  dispatch(setView(views.ERROR));

  if (typeof error === 'undefined') {
    console.error('Undefined error');
    return;
  }

  console.error(`Error: ${error.type} (${error.description}). View set to ERROR`);
  dispatch({
    type: CATCH_ERROR,
    error
  });
};

export const checkCompatibility = () => (dispatch) => {
  dispatch({ type: COMPATIBILITY_CHECK_REQUEST });

  window.addEventListener('devicemotion', gyroCheck, false);

  const timeout = setTimeout(() => {
    window.removeEventListener('devicemotion', gyroCheck, false);

    if (process.env.NODE_ENV === 'development') {
      console.warn('Gyro required and not found, but continued because of development environment');
      dispatch({ type: COMPATIBILITY_CHECK_SUCCESS });
      return;
    }

    dispatch({ type: COMPATIBILITY_CHECK_FAILURE });
  }, 4000);

  window.addEventListener('gyroavailable', checkSuccess(
    timeout,
    dispatch,
    COMPATIBILITY_CHECK_SUCCESS
  ), false);
};

export const start = () => (dispatch, getState) => {
  const { flags } = getState();

  dispatch(setView(views.LOADING));

  if (!flags.compatibilityChecked) {
    dispatch(checkCompatibility());
  }
};

export const restart = () => (dispatch) => {
  dispatch({ type: RESET_ANSWERS });
  dispatch(setView(views.WELCOME));
};

export const resume = () => (dispatch, getState) => {
  const { answers } = getState();

  const answersGiven = Object.keys(answers).length;

  if (answersGiven === questions.length) {
    dispatch(answerLastQuestion());
    dispatch(setQuestion(answersGiven - 1));
  } else {
    dispatch(setQuestion(answersGiven));
  }

  dispatch(setView(views.WELCOME));
};


export const welcome = () => (dispatch, getState) => {
  const { flags, answers } = getState();

  const answersGiven = Object.keys(answers).length;

  if (flags.compatible) {
      dispatch(setStage(stages.STORY));
    if (answersGiven > 0) {
      dispatch(setView(views.RESUME));
    } else {
      dispatch(setView(views.WELCOME));
    }
  } else {
    dispatch(catchError(errors.INCOMPATIBLE));
  }
};

export const sendAnswers = () => (/* dispatch */) => {
  console.error('Not implemented yet');
};

export const setVRDisplay = () => (dispatch) => {
  navigator.getVRDisplays()
    .then((displays) => {
      if (displays.length > 0) {
        dispatch({
          type: SET_VRDISPLAY,
          display: displays[0]
        });
      } else {
        dispatch(catchError(errors.VR_NOTFOUND));
      }
    }).catch(() => { dispatch(catchError(errors.VR_NOTFOUND)); });
};
