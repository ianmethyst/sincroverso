const SETTINGS = {
  online: false,
  server: {
    ip: '127.0.0.1',
    port: 8080
  },
  mobileOnly: false
};

export default SETTINGS;
