import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { get } from 'lodash';

import {
  catchError,
  setVRDisplay,
  togglePresent,
  welcome
} from './actions/creators';

import GlobalStyle from './components/GlobalStyle';
import AspectRatio from './components/AspectRatio';
import TransitionGroup from './components/TransitionGroup';
import ViewContainer from './components/ViewContainer';

import Three from './vr';

import views, { components } from './views';
import stages from './data/appStages';
import errors from './data/errorTypes';

class App extends React.PureComponent {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    dispatch(setVRDisplay());

    this.state = {
      height: window.innerHeight * 0.01,
    };

    this.setHeight = this.setHeight.bind(this);
    this.handleKey = this.handleKey.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.setHeight, false);
    window.addEventListener('keydown', this.handleKey, false);
  }

  componentDidUpdate() {
    const {
      stage,
      compatibilityChecked,
      modelsLoaded,
      dispatch
    } = this.props;

    if (stage === stages.INIT && compatibilityChecked && modelsLoaded) {
      dispatch(welcome());
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setHeight, false);
    window.removeEventListener('keydown', this.handleKey, false);
  }

  setHeight() { this.setState({ height: window.innerHeight * 0.01 }); }

  getKey() {
    const { view, currentQuestion } = this.props;

    if (view === views.QUESTION) {
      return `${view}-${currentQuestion}`;
    }
    return view;
  }

  handleKey(e) {
    if (e.key === 'Escape') {
      const { dispatch, presenting } = this.props;
      if (presenting) {
        dispatch(togglePresent());
      }
    }
  }

  renderView() {
    const { presenting } = this.props;

    // TODO: also remove aspect ratio element, maybe
    if (presenting) return null;

    const { dispatch, view } = this.props;

    const element = get(components, view);

    if (typeof element === 'undefined') {
      dispatch(catchError(errors.UNDEFINED_VIEW));
      return null;
    }

    return element;
  }

  render() {
    const { height } = this.state;
    const { vrDisplay } = this.props;
    const key = this.getKey();

    return (
      <>
        <GlobalStyle height={height} />
        { vrDisplay ? <Three /> : null }
        <AspectRatio ratio="9:16" show={true}>
          <TransitionGroup key={key}>
            <ViewContainer>
              {this.renderView()}
            </ViewContainer>
          </TransitionGroup>
        </AspectRatio>
      </>
    );
  }
}

App.propTypes = {
  stage: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  presenting: PropTypes.bool.isRequired,
  vrDisplay: PropTypes.instanceOf(window.VRDisplay),
  view: PropTypes.string.isRequired,
  currentQuestion: PropTypes.number.isRequired,
  compatibilityChecked: PropTypes.bool.isRequired,
  modelsLoaded: PropTypes.bool.isRequired,
  answers: PropTypes.shape({}).isRequired
};

App.defaultProps = {
  vrDisplay: {}
};

const mapStateToProps = state => ({
  stage: state.flags.appStage,
  compatibilityChecked: state.flags.compatibilityChecked,
  modelsLoaded: state.vr.models.loaded,
  presenting: state.vr.presenting,
  vrDisplay: state.vr.vrDisplay,
  view: state.view.view,
  currentQuestion: state.view.currentQuestion,
  answers: state.answers
});

export default connect(mapStateToProps)(App);
