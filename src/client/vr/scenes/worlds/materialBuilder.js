import * as THREE from 'three';

import {
  biomes,
  materialNames,
  materialColors
} from '../../../data/worldParameters';

export function assignMaterial(object, name, material) {
  const o = object;
  if (object.material && object.material.name === name) {
    o.material = material;
  }
}

function buildMaterial(name, color, opacity = 1) {
  const material = new THREE.MeshStandardMaterial({
    name,
    roughness: 0.75,
    metalness: 0,
    flatShading: true,
    transparent: opacity !== 1,
    opacity,
    color
  });
  return material;
}

export function buildAllMaterials(answers) {
  const {
    FLORA_COLOR,
    ATMOSPHERE,
    BIOME
  } = answers;

  let earth;
  let grass;

  switch (BIOME) {
    case biomes.GRASSY:
      grass = buildMaterial(materialNames.GRASS, FLORA_COLOR);
      earth = buildMaterial(materialNames.GROUND, new THREE.Color(FLORA_COLOR).offsetHSL(0, 0, -0.2));
      break;
    case biomes.ROCKY:
      grass = buildMaterial(materialNames.GRASS, materialColors.ROCK);
      earth = buildMaterial(materialNames.GROUND, new THREE.Color(materialColors.ROCK).offsetHSL(0, 0, -0.2));
      break;
    case biomes.DESERTIC:
      grass = buildMaterial(materialNames.GRASS, materialColors.SAND);
      earth = buildMaterial(materialNames.GROUND, new THREE.Color(materialColors.SAND).offsetHSL(0, 0, -0.2));
      break;
    default:
      console.error('Failed while creating material: biome not defined');
  }

  const materials = {
    grass,
    earth,
    water: buildMaterial(materialNames.WATER, ATMOSPHERE),
    leaf: grass,
    snow: buildMaterial(materialNames.SNOW, materialColors.SNOW),
    rock: buildMaterial(materialNames.ROCK, materialColors.ROCK),
  };

  return materials;
}
