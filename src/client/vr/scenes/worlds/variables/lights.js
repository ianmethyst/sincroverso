import * as THREE from 'three';

import { map, parseMins } from '../../../common/util';

export function buildLights(atmosphereColor, worldSize) {
  // Despite the name, this function creates both the sun, moon and helper lights
  //
  // This will be used to pivot the sun and moon arround the world
  const anchor = new THREE.Group();

  const sun = new THREE.SpotLight(
    0xffffff, // color
    0.8, // intensity
    0, // distance
    0.86, // angle
    1, // penumbra
    1, // decay
  );
  sun.name = 'sun';
  sun.castShadow = true;
  sun.shadow.mapSize.width = 4096;
  sun.shadow.mapSize.height = 4096;

  const moon = new THREE.SpotLight(
    0x17419a, // color
    0.8, // intensity
    0, // distance
    0.12, // angle
    1, // penumbra
    1, // decay
  );
  moon.name = 'moon';
  moon.castShadow = true;
  moon.shadow.mapSize.width = 4096;
  moon.shadow.mapSize.height = 4096;

  // This lights will be used to unflatten the world shape, and give it the atmosphere color
  const helper1 = new THREE.PointLight(new THREE.Color(atmosphereColor).offsetHSL(0, -0.5, 0), 0.4);
  const helper2 = new THREE.PointLight(new THREE.Color(atmosphereColor).offsetHSL(0, -0.5, 0), 0.4);

  // Positions
  const distance = 100;
  sun.position.set(0, worldSize + distance, 0);
  moon.position.set(0, -worldSize - distance, 0);
  helper1.position.set(0, 0, worldSize + distance);
  helper2.position.set(0, 0, -worldSize - distance);

  // Add everything to the anchor
  anchor.add(sun);
  anchor.add(moon);
  anchor.add(helper1);
  anchor.add(helper2);

  /*
  // Add meshes to see where the light is coming from (debugging)
  const geometry = new THREE.OctahedronBufferGeometry(2, 0);
  const material = new THREE.MeshBasicMaterial({ color: 0xffffff });

  const sunMesh = new THREE.Mesh(geometry, material);
  const moonMesh = new THREE.Mesh(geometry, material);
  sunMesh.position.copy(sun.position);
  moonMesh.position.copy(moon.position);
  anchor.add(sunMesh);
  anchor.add(moonMesh);
  */

  return anchor;
}

export function setLightsPosition(daytime) {
  // A day has 1440 minutes, hence the max of the range to map
  const sunPosition = map(parseMins(daytime), 0, 1439, 0, 2 * Math.PI, true) - Math.PI;
  return sunPosition;
}

/*
  // Dont use ambient light to avoid flattening
function buildAmbientLight(atmosphereColor) {
  const light = new THREE.AmbientLight(atmosphereColor, 0.1);

  return light;
}
*/
