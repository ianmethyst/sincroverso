import * as THREE from 'three';
import { get, pick, sample } from 'lodash';

import { floraPositions } from '../../../../data/objectPositions';

import {
  biomeFlora
} from '../../../../data/worldParameters';

function buildFlora(answers, models, surface) {
  const {
    BIOME,
    TERRAIN,
    SEASON
  } = answers;

  const biome = get(biomeFlora, BIOME);
  const flora = pick(models, biome);
  const positions = get(floraPositions, TERRAIN);

  const container = new THREE.Group();

  positions.forEach((position) => {
    const anchor = new THREE.Group();

    const tree = get(sample(flora), 'scene').clone();
    tree.position.set(0, surface / 2, 0);
    tree.scale.multiplyScalar(2);

    tree.castShadow = true;
    tree.receiveShadow = true;

    anchor.add(tree);
    anchor.rotation.fromArray(position);

    container.add(anchor);
  });

  return container;
}

export default buildFlora;
