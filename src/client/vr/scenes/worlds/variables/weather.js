import * as THREE from 'three';
import SPE from '../../../vendor/shader-particle-engine';

import { seasons } from '../../../../data/worldParameters';

import snow from './particles/snow';
import rain from './particles/rain';

function buildParticleGroup(texture) {
  const particleGroup = new SPE.Group({
    texture: {
      value: texture
    },
    fog: true,
    maxParticleCount: 30000
  });

  return particleGroup;
}

function buildWeather(season, color = 0x0000ff) {
  let texture;
  let emitter;

  switch (season) {
    case seasons.SUMMER: {
      texture = new THREE.TextureLoader().load('./texture/raindrop.png');
      texture.flipY = false;
      emitter = new SPE.Emitter(rain(color));
      break;
    }
    case seasons.WINTER: {
      texture = new THREE.TextureLoader().load('./texture/snowflake.png');
      emitter = new SPE.Emitter(snow());
      break;
    }
    default: {
      console.error('Weather not defined!');
    }
  }

  const particleGroup = buildParticleGroup(texture);

  if (emitter) {
    particleGroup.addEmitter(emitter);
    return particleGroup;
  }

  return null;
}

export default buildWeather;
