import {
  seasons,
  materialNames
} from '../../../../data/worldParameters';

import { assignMaterial } from '../materialBuilder';

function setBiome(answers, world, flora, materials) {
  const { SEASON } = answers;

  world.traverse((child) => {
    const c = child;

    if (SEASON === seasons.WINTER) {
      assignMaterial(c, materialNames.GRASS, materials.snow);
    } else {
      assignMaterial(c, materialNames.GRASS, materials.grass);
    }
    assignMaterial(c, materialNames.WATER, materials.water);
    assignMaterial(c, materialNames.EARTH, materials.earth);
  });

  flora.traverse((child) => {
    const c = child;

    assignMaterial(c, materialNames.LEAF, materials.leaf);
    if (SEASON === seasons.WINTER) {
      assignMaterial(c, materialNames.SNOW, materials.snow);
    } else {
      assignMaterial(c, materialNames.SNOW, materials.leaf);
    }
  });
}

export default setBiome;
