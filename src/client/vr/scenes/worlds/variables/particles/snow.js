import * as THREE from 'three';

const snow = () => (
  {
    maxAge: {
      value: 20
    },
    position: {
      value: new THREE.Vector3(2, 5, 2),
      spread: new THREE.Vector3(12, 0, 12)
    },
    acceleration: {
      value: new THREE.Vector3(0, -0.2, 0),
    },
    velocity: {
      value: new THREE.Vector3(0, -0.5, 0),
      spread: new THREE.Vector3(2.5, -0.01, 2.5)
    },
    color: {
      value: [new THREE.Color(0xffffff)]
    },
    opacity: {
      value: [0.8, 0.8]
    },
    rotation: {
      value: [-1, -10]
    },
    size: {
      value: [0.1, -0.01],
      spread: [0.05, 0.1]
    },
    activeMultiplier: 0.5,
    particleCount: 30000
  }
);

export default snow;
