import * as THREE from 'three';

const rain = color => (
  {
    maxAge: {
      value: 5
    },
    position: {
      value: new THREE.Vector3(0, 5, 0),
      spread: new THREE.Vector3(9, -5, 9)
    },
    acceleration: {
      value: new THREE.Vector3(0, -2.5, 0),
    },
    velocity: {
      value: new THREE.Vector3(0, -2.5, 0),
      spread: new THREE.Vector3(0.5, -0.01, 0.2)
    },
    color: {
      value: [new THREE.Color(color)]
    },
    opacity: {
      value: [0.8, 0.8]
    },
    rotation: {
      value: [-1, -10]
    },
    size: {
      value: [0.1, -0.01],
      spread: [0.05, 0.1]
    },
    activeMultiplier: 0.5,
    particleCount: 30000
  }
);

export default rain;
