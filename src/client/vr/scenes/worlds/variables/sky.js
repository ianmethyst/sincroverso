import * as THREE from 'three';

import {
  parseMins,
  constrain,
  map
} from '../../../common/util';


function setSkyColor(scene, atmosphereColor, daytime) {
  const s = scene;

  const aColor = new THREE.Color(atmosphereColor);

  // Assign the HSL from atmosphereColor to an empty object
  const colorHSL = {};
  aColor.getHSL(colorHSL);

  // Get the lightness
  const { l } = colorHSL;

  // Limit the maximum and minimum lightness
  const maxL = constrain(l, 0.1, 0.4);

  // Map the ligtheness
  let skyLightness;
  if (parseMins(daytime) < 719) {
    skyLightness = map(parseMins(daytime), 0, 719, 0.05, maxL, true);
  } else {
    skyLightness = map(parseMins(daytime), 720, 1439, maxL, 0.05, true);
  }

  const skyColor = new THREE.Color(aColor).setHSL(colorHSL.h, colorHSL.s, skyLightness);

  s.background = skyColor;
  s.fog = new THREE.Fog(skyColor, 0, 325);

  return skyColor;
}

export default setSkyColor;
