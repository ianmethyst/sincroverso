import * as THREE from 'three';
import { get, sample } from 'lodash';

import { fauna } from '../../../../data/worldParameters';

function buildAnimal(answer, models, surface) {
  const anchor = new THREE.Group();

  const animals = fauna[answer];
  const sampled = sample(animals);
  const animal = get(models, [sampled.key, 'scene']);
  animal.traverse((child) => {
    const c = child;
    if (c.material) {
      c.material.side = THREE.DoubleSide;
      c.material.roughness = 1;
      c.material.metalness = 0;
    }
  });

  animal.scale.multiplyScalar(sampled.scale);
  animal.position.set(0, surface / 2, 0);
  animal.rotation.set(0, Math.PI * 1.25, 0);

  anchor.add(animal);
  anchor.rotation.set(0.0, -0.0, 0);

  return anchor;
}

export default buildAnimal;
