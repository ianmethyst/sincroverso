import * as THREE from 'three';
import { get } from 'lodash';

function buildTerrain(terrain, models, worldSize) {
  const model = get(models, [terrain, 'scene']);
  const world = model.clone();
  world.scale.multiplyScalar(worldSize);

  // Get surface object
  let grassGeometry;
  world.traverse((child) => {
    if (child.name === 'grassGeometry') {
      grassGeometry = child;
    }
  });

  // Size of surface object
  const worldBox = new THREE.Box3().setFromObject(grassGeometry);
  const surface = worldBox.max.y * worldSize - worldBox.min.y * worldSize;

  world.castShadow = true;
  world.receiveShadow = true;

  return { world, surface };
}

export default buildTerrain;
