import * as THREE from 'three';

import buildWorld from './worldBuilder';
import setSkyColor from './variables/sky';

export function buildScene(answers, models) {

  Object.values(models).forEach((model) => {
    const { scene } = model;
    scene.traverse((child) => {
      const c = child;

      if (c instanceof THREE.Mesh) {
        c.castShadow = true;
        c.receiveShadow = true;
      }
    });
  });

  const scene = new THREE.Scene();

  setSkyColor(scene, answers.ATMOSPHERE, answers.DAYTIME);

  const world = buildWorld(answers, models);
  scene.particleGroup = world.particleGroup;
  scene.add(world);

  return scene;

  /*
    setBiome(sample(biomes), northFlora, northWorld);
    changeColor(northWorld, 'water', northAtmosphere);
    setBiome(sample(biomes), eastFlora, eastWorld);
    changeColor(eastWorld, 'water', eastAtmosphere);
    setBiome(sample(biomes), westFlora, westWorld);
    changeColor(westWorld, 'water', westAtmosphere);

    eastWorld.position.set(0, 90, 150);
    westWorld.position.set(150, 90, 0);
    northWorld.position.set(150, 90, 150);


    setBiome(biome, floraColor, world, season);
    changeColor(world, 'water', atmosphereColor);

// flora
    if (terrain === terrains.PLAIN) {
      const tree1 = buildFlora(models, terrain, biome, floraColor, surface, false, season);
      const tree2 = tree1.clone();
      const tree3 = tree1.clone();
      const tree4 = tree1.clone();
      const tree5 = tree1.clone();

      tree1.rotation.set(0.1, 0, -0.3);
      tree2.rotation.set(-0.3, 0, -0.6);
      tree3.rotation.set(0.8, 0, 0);
      tree4.rotation.set(-0.1, 0, 0.7);
      tree5.rotation.set(-0.6, 0, 0.2);
      container.add(tree1);
      container.add(tree2);
      container.add(tree3);
      container.add(tree4);
      container.add(tree5);

      if (biome === biomes.ROCKY) {
        const bush = buildFlora(models, terrain, biome, floraColor, surface, true);
        bush.rotation.set(-0.3, 0, -0.4);
        bush.children[0].position.set(0, surface / 2 - 0.2, 0);
        container.add(bush);
      }
    } else if (terrain === terrains.ISLE) {
      const tree1 = buildFlora(models, terrain, biome, floraColor, surface, false);
      const tree2 = tree1.clone();
      const tree3 = tree1.clone();


      tree1.rotation.set(0.4, 0, -0.2);
      tree2.rotation.set(0.1, 0, 0.4);
      tree3.rotation.set(0.3, 0, 0.2);

      container.add(tree1);
      container.add(tree2);
      container.add(tree3);
    } else if (terrain === terrains.MOUNTAIN) {
      const tree1 = buildFlora(models, terrain, biome, floraColor, surface, false);
      const tree2 = tree1.clone();
      const tree3 = tree1.clone();

      tree1.rotation.set(0.1, 0, -0.3);
      tree2.rotation.set(-0.4, 0, 0.2);
      tree3.rotation.set(0, 0, 0.7);

      container.add(tree1);
      container.add(tree2);
      container.add(tree3);
    }
  });

  scene.add(container);
  scene.name = 'worldsScene';

  return scene;
  */
}
