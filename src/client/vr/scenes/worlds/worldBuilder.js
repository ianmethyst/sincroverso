import * as THREE from 'three';

import buildTerrain from './variables/terrain';
import { buildLights, setLightsPosition } from './variables/lights';
import buildAnimal from './variables/animal';
import buildWeather from './variables/weather';
import buildFlora from './variables/flora';
import { buildAllMaterials } from './materialBuilder';
import setBiome from './variables/biome';

import { setObjectPosition } from '../../common/util';
import { animalPositions } from '../../../data/objectPositions';

import { weathers } from '../../../data/worldParameters';

const worldSize = 25;

const buildWorld = (answers, models) => {
  const container = new THREE.Group();

  // Terrain
  const { world, surface } = buildTerrain(answers.TERRAIN, models, worldSize);
  container.add(world);

  // Lights
  const lights = buildLights(answers.ATMOSPHERE, worldSize);
  lights.rotation.set(setLightsPosition(answers.DAYTIME), 0, 0);
  lights.getObjectByName('sun').lookAt(container.position);
  lights.getObjectByName('moon').lookAt(container.position);
  container.add(lights);

  // Animals
  const animal = buildAnimal(answers.FAUNA, models, surface);
  setObjectPosition(animal, animalPositions, answers.TERRAIN);
  container.add(animal);

  // Weather
  if (answers.WEATHER === weathers.WET) {
    const particleGroup = buildWeather(answers.SEASON, answers.ATMOSPHERE);
    container.add(particleGroup.mesh);
    particleGroup.mesh.position.set(0, surface / 2, 0);
    particleGroup.tick(16);
    container.particleGroup = particleGroup;
  }

  // Flora
  const flora = buildFlora(answers, models, surface);
  container.add(flora);

  // Biome
  const materials = buildAllMaterials(answers);
  setBiome(answers, world, flora, materials);

  // Position
  container.position.set(0, -worldSize / 2, 0);

  return container;
};

export default buildWorld;
