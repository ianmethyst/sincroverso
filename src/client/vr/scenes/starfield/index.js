import * as THREE from 'three';
//import { buildFont, buildText } from './text';

function buildStarfield() {
  const geometry = new THREE.OctahedronBufferGeometry(1.2, 0);
  const material = new THREE.MeshBasicMaterial({ color: 0xffffff });
  const group = new THREE.Group();
  for (let i = 0; i < 1000; i += 1) {
    const parent = new THREE.Object3D();
    const mesh = new THREE.Mesh(geometry, material);
    mesh.position.set(0, 0, Math.random() * 500 + 150);
    parent.add(mesh);
    parent.rotation.set(
      Math.random() * Math.PI * 2,
      Math.random() * Math.PI * 2,
      Math.random() * Math.PI * 2
    );
    group.add(parent);
  }

  return group;
}

/*
 
export function buildButton() {
  const geometry = new THREE.CircleBufferGeometry(0.10, 32);
  const material = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    transparent: true,
    opacity: 0.25
  });

  const mesh = new THREE.Mesh(geometry, material);
  return mesh;
}

export function buildMessage(scene, cursor, cb) {
  //  Add everything to scene after loading font
  buildFont()
    .then((font) => {
      const button = buildButton();
      button.position.set(0, 1.35, -0.75);
      button.lookAt(0, 1.6, 0);

      const message = buildText(
        'Para viajar al sincroverso, mirá fijamente el circulo',
        font
      );
      message.position.set(0, 1.6, -0.75);
      scene.add(message);

      const buttonText = buildText('ENTRAR', font);
      buttonText.position.set(0, 0, 0.01);
      button.add(buttonText);

      scene.add(button);
      cursor.add(button, buttonConfig);
    })
    .catch((error) => {
      console.error(error);
    });
}

  */

export function buildScene() {
  const scene = new THREE.Scene({
    background: new THREE.Color('#000000')
  });

  const starfield = buildStarfield();
  scene.add(starfield);

  return scene;
}
