import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import {
  setRenderer,
  catchError,
  loadModelsRequest,
  loadModelsSuccess,
  loadModelsFailure
} from '../actions/creators';

import anime from 'animejs';

import errors from '../data/errorTypes';

import {
  buildRenderer,
  buildCamera,
  buildClock,
  buildCurtain,
} from './common/basics';

import PointerLockControls from '../vr/vendor/three-pointer-lock';
import modelLoader from '../vr/common/modelLoader';
import * as starfieldScene from './scenes/starfield';
import * as worldsScene from './scenes/worlds';

class Three extends React.PureComponent {
  constructor(props) {
    super(props);

    this.animate = this.animate.bind(this);
    this.onWindowResize = this.onWindowResize.bind(this);
    this.onVREnter = this.onVREnter.bind(this);
    this.handleClick = this.handleClick.bind(this);

    this.mount = React.createRef();

    this.state = {
      debug: false
    };
  }

  componentDidMount() {
    this.dimensions = {
      width: this.mount.current.clientWidth,
      height: this.mount.current.clientHeight
    };

    window.addEventListener('vrdisplaypresentchange', this.onVREnter, false);
    window.addEventListener('resize', this.onWindowResize, false);

    this.loadModels();
  }

  componentWillUnmount() {
    this.stop();
    this.mount.removeChild(this.renderer.domElement);
  }

  onVREnter() {
    const { flags } = this.props;

    if (!flags.immersionBegan && flags.allAnswered) {
      console.log('¿Mmm?');
    }
  }

  onWindowResize() {
    this.dimensions = {
      width: this.mount.current.clientWidth,
      height: this.mount.current.clientHeight
    };

    const { width, height } = this.dimensions;

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
  }

  loadModels() {
    const { dispatch } = this.props;

    dispatch(loadModelsRequest());
    this.models = {};

    Promise.all(modelLoader()).then((m) => {
      m.forEach((model) => { this.models = Object.assign({ ...this.models }, model); });
      dispatch(loadModelsSuccess());
      this.init();
    }).catch((error) => {
      console.error(error);
      dispatch(catchError(errors.THREE_ERROR));
    });
  }

  init() {
    const { dispatch, vr, sandbox } = this.props;

    this.camera = buildCamera(this.dimensions);
    this.camera.position.set(0, 1.6, 0);
    this.renderer = buildRenderer(this.dimensions, vr.vrDisplay);
    dispatch(setRenderer(this.renderer));
    this.clock = buildClock();
    this.curtain = buildCurtain(0);
    this.camera.add(this.curtain);
    this.controls = new PointerLockControls(this.camera);

    if (sandbox) {
      this.changeScene(worldsScene);
    } else {
      this.changeScene(starfieldScene);
    }

    this.scene.add(this.controls.getObject());

    // Append canvas
    this.mount.current.appendChild(this.renderer.domElement);

    this.start();
  }

  handleClick() {
    this.controls.lock();
  }

  changeScene(scene) {
    const { answers } = this.props;

    if (!this.scene) {
      this.scene = scene.buildScene(answers, this.models);
    } else {
      // TODO: Transition with curtain
    }
  }

  start() { this.renderer.setAnimationLoop(this.animate); }

  stop() { this.renderer.setAnimationLoop(null); }

  animate() {
    const delta = this.clock.getDelta();

    if (this.scene.particleGroup) {
      this.scene.particleGroup.tick(delta);
    }

    this.renderer.render(this.scene, this.camera);
  }

  render() {
    return (
      <div
        id="three"
        style={{ position: 'absolute', width: '100%', height: '100%' }}
        ref={this.mount}
        onClick={this.handleClick}
      />
    );
  }
}

Three.propTypes = {
  sandbox: PropTypes.bool,
  answers: PropTypes.shape({}).isRequired,
  vr: PropTypes.shape({}).isRequired,
  flags: PropTypes.shape({
    immersionBegan: PropTypes.bool.isRequired
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
};

Three.defaultProps = {
  sandbox: false
};

const mapStateToProps = state => ({
  flags: state.flags,
  answers: state.answers,
  vr: state.vr
});

export default connect(mapStateToProps)(Three);
