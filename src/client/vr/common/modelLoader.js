import GLTFLoader from 'three-gltf-loader';

import resources from '../../data/resources';

function modelLoader() {
  const loader = new GLTFLoader();

  const models = resources.map(res => (
    new Promise((resolve, reject) => {
      loader.load(res.resource, (object) => {
        const obj = {};
        obj[res.key] = object;
        resolve(obj);
      }, (xhr) => {
        /* console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' ) */
      }, (error) => {
        console.log('An error happened while loading models');
        reject(error);
      });
    })
  ));

  return models;
}

export default modelLoader;
