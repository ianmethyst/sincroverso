import { get } from 'lodash';

export function parseMins(hour) {
  // Take a string with the format "xx:xx". Multiply the first number by 60 and add the second one
  const splitted = hour.split(':');
  const minutes = parseInt(splitted[0] * 60, 10) + parseInt(splitted[1], 10);

  return minutes;
}

export function constrain(n, low, high) {
  // Function from p5.js to constrain a number between a range
  return Math.max(Math.min(n, high), low);
}

export function map(n, start1, stop1, start2, stop2, withinBounds) {
  // Function from p5.js to map a number from a range to a different one
  const newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
  if (!withinBounds) {
    return newval;
  }
  if (start2 < stop2) {
    return constrain(newval, start2, stop2);
  }

  return constrain(newval, stop2, start2);
}

export function getRandomHex() {
  const randomColor = '#000000'.replace(/0/g, () => ((~~(Math.random() * 16)).toString(16)));
  return randomColor;
}

// This is used to set an object position and rotation around the globe
export function setObjectPosition(object, positions, terrain, index = null) {
  const data = get(positions, terrain);

  if (data.position) {
    object.position.fromArray(data.position);
  }
  if (data.rotation) {
    if (!index) {
      object.rotation.fromArray(data.rotation);
    } else {
      object.rotation.fromArray(data.rotation[index]);
    }
  }
}
