import * as THREE from 'three';
import loadFont from 'load-bmfont';
import createGeometry from '../vendor/three-bmfont-text';
import shader from '../vendor/three-bmfont-text/shaders/msdf';

export function buildFont() {
  return new Promise((resolve) => {
    loadFont('font/Raleway-Regular-msdf.json', (err, font) => {
      if (err) throw err;

      const textureLoader = new THREE.TextureLoader();
      textureLoader.load('font/Raleway-Regular-i.png', (texture) => {
        const f = { font, texture };
        resolve(f);
      });
    });
  });
}

export function buildText(text, f) {
  const { font, texture } = f;
  const geometry = createGeometry({
    width: 600,
    align: 'center',
    font
  });

  const material = new THREE.RawShaderMaterial(shader({
    map: texture,
    side: THREE.DoubleSide,
    transparent: true,
    color: 0xffffff
  }));

  const txt = new THREE.Mesh(geometry, material);

  geometry.update(text);

  const { layout } = geometry;
  txt.position.x = -layout.width / 2;
  txt.position.y = layout.height / 2;


  const anchor = new THREE.Object3D();
  anchor.scale.multiplyScalar(0.001);
  anchor.add(txt);
  anchor.rotation.set(Math.PI, 0, 0);

  return anchor;
}
