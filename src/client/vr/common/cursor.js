import * as THREE from 'three';

// Old test for reticle and raycaster

export function buildReticle() {
  const geometry = new THREE.RingBufferGeometry(0.03, 0.06);
  const material = new THREE.MeshBasicMaterial({ color: 0x00ffff, side: THREE.DoubleSide });

  const reticle = new THREE.Mesh(geometry, material);
  reticle.position.z = -5;

  return reticle;
}

export function buildRaycaster(camera) {
  const raycaster = new THREE.Raycaster();

  raycaster.far = 20;
  raycaster.wp = new THREE.Vector3();
  raycaster.wd = new THREE.Vector3(0, 0, -1);

  raycaster.update = function update() {
    raycaster.set(camera.getWorldPosition(this.wp), camera.getWorldDirection(this.wd));
  };

  raycaster.intersected = [];

  raycaster.test = function test(scene) {
    this.intersectObjects(scene.children, false, this.intersected);

    this.intersected.map(i => (
      console.log(i)
    ));
  };

  const r = Object.assign(raycaster, THREE.EventDispatcher.prototype);
  console.log(r);

  return r;
}
