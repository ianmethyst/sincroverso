import * as THREE from 'three';

export function buildCamera(dimensions) {
  const { width, height } = dimensions;
  const camera = new THREE.PerspectiveCamera(50, width / height, 0.1, 2000);
  return camera;
}

export function buildRenderer(dimensions, device) {
  const { width, height } = dimensions;
  const renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setSize(width, height);

  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;

  renderer.vr.enabled = true;
  renderer.vr.setDevice(device);

  renderer.gammaOutput = true;
  renderer.gammaFactor = 2.2;

  return renderer;
}

export function buildClock() {
  const clock = new THREE.Clock(true);
  return clock;
}


export function buildCurtain(opacity) {
  const geometry = new THREE.PlaneBufferGeometry(10, 10);
  const material = new THREE.MeshBasicMaterial({
    color: 0x000000,
    depthTest: false,
    transparent: true,
    opacity
  });

  const mesh = new THREE.Mesh(geometry, material);
  mesh.renderOrder = 999;
  mesh.position.set(0, 0, -0.2);
  return mesh;
}
