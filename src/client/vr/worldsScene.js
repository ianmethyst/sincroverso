import * as THREE from 'three';
import GLTFLoader from 'three-gltf-loader';
import get from 'lodash/get';
import sample from 'lodash/sample';
// import { buildFont, buildText } from './text';

import resources from '../data/resources';
import {
  biomes,
  terrains,
  fauna as FAUNA,
  predefinedColors,
  seasons,
  weathers
} from '../data/worldParameters';

const worldSize = 25;

function changeColor(object, material, color) {
  object.traverse((child) => {
    const c = child;
    if (c.material && c.material.name === material) {
      // const c = new THREE.Color().setHSL(Math.random(), 0.5, 0.5)
      c.material = new THREE.MeshStandardMaterial({
        name: material,
        roughness: 0.75,
        metalness: 0,
        flatShading: true,
        color
      });
    }
  });
}

export function modelLoader() {
  const loader = new GLTFLoader();

  const models = resources.map((res) => {
    return new Promise((resolve, reject) => {
      loader.load(res.resource, (object) => {
        const obj = {};
        obj[res.key] = object;
        resolve(obj);
      }, (xhr) => {
        /* console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' ) */
      }, (error) => {
        console.log('An error happened while loading models');
        reject(error);
      });
    });
  });

  return models;
}

/*
  // Dont use ambient light to avoid flattening
function buildAmbientLight(atmosphereColor) {
  const light = new THREE.AmbientLight(atmosphereColor, 0.1);

  return light;
}
*/

export const testAnswers = {
  terrain: terrains.MOUNTAIN,
  biome: biomes.GRASSY,
  fauna: FAUNA.CARNIVORE,
  daytime: '14:00',
  atmosphereColor: '#1cd1fe',
  floraColor: '#ff00ff'
};

function buildWorld(models, terrain) {
  const model = get(models, [terrain, 'scene']);
  const world = model.clone();
  return world;
}



export function buildScene(answers) {
  const terrain = answers.TERRAIN;
  const biome = answers.BIOME;
  const fauna = answers.FAUNA;
  const atmosphereColor = answers.ATMOSFERE_COLOR;
  const daytime = answers.DAYTIME;
  const floraColor = answers.FLORA_COLOR;
  const weather = answers.WEATHER;
  const season = answers.SEASON;

  // Create the scene and set sky color
  const scene = new THREE.Scene();
  const skyColor = setSkyColor(atmosphereColor, daytime);
  scene.background = skyColor;
  scene.fog = new THREE.Fog(skyColor, 0, 325);

  // Create container for own world
  const container = new THREE.Group();


  // Load models and do manipulations here
  Promise.all(modelLoader()).then((m) => {
    let models = {};
    m.forEach((object) => { models = Object.assign({ ...models }, object); });

    console.log(models);

    const world = buildWorld(models, terrain);

    const northWorld = buildWorld(models, sample(terrains));
    const eastWorld = buildWorld(models, sample(terrains));
    const westWorld = buildWorld(models, sample(terrains));

    const eastFlora = getRandomHex();
    const eastAtmosphere = getRandomHex();
    const westFlora = getRandomHex();
    const westAtmosphere = getRandomHex();
    const northFlora = getRandomHex();
    const northAtmosphere = getRandomHex();
    setBiome(sample(biomes), northFlora, northWorld);
    changeColor(northWorld, 'water', northAtmosphere);
    setBiome(sample(biomes), eastFlora, eastWorld);
    changeColor(eastWorld, 'water', eastAtmosphere);
    setBiome(sample(biomes), westFlora, westWorld);
    changeColor(westWorld, 'water', westAtmosphere);

    northWorld.scale.multiplyScalar(worldSize);
    eastWorld.scale.multiplyScalar(worldSize);
    westWorld.scale.multiplyScalar(worldSize);

    eastWorld.position.set(0, 90, 150);
    westWorld.position.set(150, 90, 0);
    northWorld.position.set(150, 90, 150);

    scene.add(northWorld);
    scene.add(eastWorld);
    scene.add(westWorld);

    world.scale.multiplyScalar(worldSize);

    let grassGeometry;
    world.traverse((child) => {
      const c = child;
      if (c.name === 'grassGeometry') {
        grassGeometry = c;
      }
    });

    const worldBox = new THREE.Box3().setFromObject(grassGeometry);

    const surface = worldBox.max.y * worldSize - worldBox.min.y * worldSize;

    // Create rain
    if (weather === weathers.WET) {
      const particleGroup = buildWeather(season, atmosphereColor);
      container.add(particleGroup.mesh);
      particleGroup.mesh.position.set(0, surface / 2, 0);
      particleGroup.tick(16);
      scene.particleGroup = particleGroup;
    }


    container.position.set(0, -surface / 2, 0);

    setBiome(biome, floraColor, world, season);
    changeColor(world, 'water', atmosphereColor);

    container.add(world);

    const animal = buildAnimal(models, fauna, surface);

    // This is used to set a static position for the animals
    // Should be removed when physics are implemented
    
    if (terrain === terrains.ISLE) {
      animal.position.set(0, 0.5, 0);
      animal.rotation.set(0.3, 0, 0);
    } else if (terrain === terrains.MOUNTAIN) {
      animal.position.set(0, 0.25, 0);
      animal.rotation.set(0.3, -0.2, 0.1);
    } else {
      animal.position.set(0, 0, 0);
      animal.rotation.set(0.1, 0, 0.3);
      animal.children[0].rotation.set(0, 1, 0);
    }

    container.add(animal);

    // flora
    if (terrain === terrains.PLAIN) {
      const tree1 = buildFlora(models, terrain, biome, floraColor, surface, false, season);
      const tree2 = tree1.clone();
      const tree3 = tree1.clone();
      const tree4 = tree1.clone();
      const tree5 = tree1.clone();

      tree1.rotation.set(0.1, 0, -0.3);
      tree2.rotation.set(-0.3, 0, -0.6);
      tree3.rotation.set(0.8, 0, 0);
      tree4.rotation.set(-0.1, 0, 0.7);
      tree5.rotation.set(-0.6, 0, 0.2);
      container.add(tree1);
      container.add(tree2);
      container.add(tree3);
      container.add(tree4);
      container.add(tree5);

      if (biome === biomes.ROCKY) {
        const bush = buildFlora(models, terrain, biome, floraColor, surface, true);
        bush.rotation.set(-0.3, 0, -0.4);
        bush.children[0].position.set(0, surface / 2 - 0.2, 0);
        container.add(bush);
      }
    } else if (terrain === terrains.ISLE) {
      const tree1 = buildFlora(models, terrain, biome, floraColor, surface, false);
      const tree2 = tree1.clone();
      const tree3 = tree1.clone();


      tree1.rotation.set(0.4, 0, -0.2);
      tree2.rotation.set(0.1, 0, 0.4);
      tree3.rotation.set(0.3, 0, 0.2);

      container.add(tree1);
      container.add(tree2);
      container.add(tree3);
    } else if (terrain === terrains.MOUNTAIN) {
      const tree1 = buildFlora(models, terrain, biome, floraColor, surface, false);
      const tree2 = tree1.clone();
      const tree3 = tree1.clone();

      tree1.rotation.set(0.1, 0, -0.3);
      tree2.rotation.set(-0.4, 0, 0.2);
      tree3.rotation.set(0, 0, 0.7);

      container.add(tree1);
      container.add(tree2);
      container.add(tree3);
    }
  });

  // Add lights
  const sun = buildSun(atmosphereColor);
  sun.rotation.set(setSunPosition(daytime), 0, 0);
  sun.getObjectByName('sun').lookAt(container.position);
  sun.getObjectByName('moon').lookAt(container.position);
  container.add(sun);

  scene.add(container);
  scene.name = 'worldsScene';

  return scene;
}
