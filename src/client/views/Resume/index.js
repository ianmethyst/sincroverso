import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { restart, resume } from '../../actions/creators';

import Text from '../../components/Text';
import Button from '../../components/Button';
import ButtonContainer from '../../components/ButtonContainer';

const text = `
  ¡Oh!, parece que ya habias estado por acá antes.
  ¿Te gustaría continuar con tus respuestas previas
  o volver a comenzar?
`;

const Resume = ({ dispatch }) => (
  <>
    <Text>
      {text}
    </Text>
    <ButtonContainer>
      <Button onClick={() => dispatch(restart())}> Recomenzar </Button>
      <Button onClick={() => dispatch(resume())}> Continuar </Button>
    </ButtonContainer>
  </>
);

Resume.propTypes = {
  dispatch: PropTypes.func.isRequired
};

export default connect()(Resume);
