import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { togglePresent } from '../../actions/creators';

import enterImage from './enter.svg';

import Title from '../../components/Title';
import Text from '../../components/Text';
import Image from '../../components/Image';
import Button from '../../components/Button';
import ButtonContainer from '../../components/ButtonContainer';

const Enter = ({ dispatch }) => (
  <>
    <Title>
      ¡Muy bien!
    </Title>
    <Text>Busca un visor Google Cardboard</Text>
    <Image src={enterImage} />
    <Text>
      {`
      Luego, rotá tu celular, pulsá el botón de entrar a RV,
      y colocá el celular dentro de éste.
      `}
    </Text>
    <ButtonContainer>
      <Button big onClick={() => dispatch(togglePresent())}> ENTRAR A RV </Button>
    </ButtonContainer>
  </>
);

Enter.propTypes = {
  dispatch: PropTypes.func.isRequired
};


export default connect()(Enter);
