import React from 'react';

import Splash from './Splash';
import Loading from './Loading';
import Error from './Error';
import Welcome from './Welcome';
import Queue from './Queue';
import EnterVR from './EnterVR';
import Confirm from './Confirm';
import Question from './Question';
import Resume from './Resume';

const constants = {
  SPLASH: 'SPLASH',
  LOADING: 'LOADING',
  ERROR: 'ERROR',
  WELCOME: 'WELCOME',
  RESUME: 'RESUME',
  QUESTION: 'QUESTION',
  CONFIRM: 'CONFIRM',
  QUEUE: 'QUEUE',
  ENTER: 'ENTER'
};

export const components = {
  SPLASH: <Splash />,
  LOADING: <Loading />,
  ERROR: <Error />,
  WELCOME: <Welcome />,
  RESUME: <Resume />,
  QUESTION: <Question />,
  CONFIRM: <Confirm />,
  QUEUE: <Queue />,
  ENTER: <EnterVR />,
};

export default constants;
