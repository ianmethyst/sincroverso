import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { start } from '../../actions/creators';

import Title from '../../components/Title';
import CenterContainer from '../../components/CenterContainer';

class Splash extends React.PureComponent {
  componentDidMount() {
    const { dispatch } = this.props;
    this.timeout = setTimeout(() => dispatch(start()), 3000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    return (
      <CenterContainer>
        <Title splash>
          Sincroverso
        </Title>
      </CenterContainer>
    );
  }
}

Splash.propTypes = {
  dispatch: PropTypes.func.isRequired
};

export default connect()(Splash);
