import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import AspectRatio from '../../components/AspectRatio';
import TransitionGroup from '../../components/TransitionGroup';
import ViewContainer from '../../components/ViewContainer';
import GlobalStyle from '../../components/GlobalStyle'


import Three from '../../vr';

import Container from './Container';
import Hamburger from './Hamburger';
import Menu from './Menu';

import { setVRDisplay } from '../../actions/creators';

class Sandbox extends React.PureComponent {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;

    dispatch(setVRDisplay());

    this.state = {
      showMenu: true
    };

    this.handleShowButton = this.handleShowButton.bind(this);
  }

  handleShowButton() {
    const { showMenu } = this.state;

    this.setState({
      showMenu: !showMenu
    });
  }

  render() {
    const { showMenu } = this.state;

    return (

      <Container>
        <GlobalStyle />
        <Three sandbox />
        <Hamburger handleClick={this.handleShowButton} showMenu={showMenu} />
        <AspectRatio ratio="9:16" show={showMenu} centered={false}>
          <TransitionGroup key="sandbox">
            <ViewContainer>
              <Menu />
            </ViewContainer>
          </TransitionGroup>
        </AspectRatio>
      </Container>
    );
  }
}

  /* const mapStateToProps = state => ({
  
});
*/

export default connect(/*mapStateToProps*/)(Sandbox);
