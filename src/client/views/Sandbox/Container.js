import styled from 'styled-components';

const Container = styled.div`
  position: relative;
  flex-direction: row;
  display: flex;
  height: 100vh;
  width: 100vw;

  font-size: 3vh;
`;

export default Container;
