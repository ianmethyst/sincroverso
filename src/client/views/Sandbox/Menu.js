import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Title from '../../components/Title';
import Button from '../../components/Button';
import ButtonContainer from '../../components/ButtonContainer';
import Form from '../../components/Form';

import questions from '../../data/questions';
import inputTypes from '../../data/inputTypes';

import {
  togglePresent
} from '../../actions/creators';

class Menu extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      inputValues: []
    };
  }

  createInputs() {
    const inputs = questions.map((question) => {
      switch (question.input) {
        case inputTypes.TEXT: {
          return null;
        }
        case inputTypes.RADIO: {
          const options = question.options;

          const values = options.map(option => {
            return <option key={option.value}> {option.value} </option>
          })

          return (
            <div key={question.value}>
              <label> {question.value} </label>
              <select>
                {values}
              </select>
            </div>
          );
        }
        case inputTypes.COLOR_PICKER: {
          return null;
        }

        default:
          console.error('Failed while creating menu: input type not defined');
          return null;
      }
    });

    return inputs;
  }

  render() {
    const { presenting, dispatch } = this.props;

    return (
      <>
        <Title> Sandbox </Title>
        <ButtonContainer>
          <Button> Randomize </Button>
        </ButtonContainer>

        <Form>
          {this.createInputs()}
        </Form>

        <ButtonContainer>
          <Button onClick={() => dispatch(togglePresent())}>
            {presenting ? 'Exit VR' : 'Enter VR'}
          </Button>
          <Button> Generate </Button>
        </ButtonContainer>
      </>
    );
  }
}

Menu.propTypes = {
  presenting: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  answers: state.anwsers,
  presenting: state.vr.presenting
});

export default connect(mapStateToProps)(Menu);
