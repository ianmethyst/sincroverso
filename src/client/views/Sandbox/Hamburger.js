import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Button from '../../components/Button';

const Override = styled(Button)`
  position: absolute;
  padding: 0;
  line-height: 1em;
  width: 2em;
  height: 2em;
  top: 0;
  left: 1em;
  z-index: 10;
`;


const Hamburger = ({ handleClick, showMenu }) => (
  <Override onClick={handleClick}>
    {showMenu ? 'X' : '☰'}
  </Override>
);

Hamburger.propTypes = {
  handleClick: PropTypes.func.isRequired,
  showMenu: PropTypes.bool.isRequired
};

export default Hamburger;
