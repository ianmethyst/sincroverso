import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { get } from 'lodash';

import {
  setView,
  setQuestion,
  addAnswer,
  answerLastQuestion
} from '../../actions/creators';

import questions from '../../data/questions';
import inputTypes from '../../data/inputTypes';
import views from '../../views';

import Button from '../../components/Button';
import ButtonContainer from '../../components/ButtonContainer';
import Form from '../../components/Form';
import FormInput from '../../components/FormInput';
import AnimatedText from '../../components/AnimatedText';

class Question extends React.Component {
  constructor(props) {
    super(props);
    const { currentQuestion } = this.props;

    const question = questions[currentQuestion];
    const inputValue = this.getSavedValue(question);

    this.state = {
      question,
      inputValue
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getSavedValue(question) {
    const { answers } = this.props;

    const answer = get(answers, question.value);
    if (answer) {
      return answer;
    }

    return '';
  }

  handleChange(e) {
    const { question } = this.state;

    if (question.input === inputTypes.COLOR_PICKER) {
      this.setState({ inputValue: e.hex });
    } else {
      this.setState({ inputValue: e.target.value });
    }
  }

  handleBack(e) {
    e.preventDefault();
    const { currentQuestion, dispatch } = this.props;

    if (currentQuestion === 0) {
      dispatch(setView(views.WELCOME));
    } else {
      dispatch(setQuestion(currentQuestion - 1));
    }
  }

  handleSubmit(e) {
    e.preventDefault();

    const { dispatch, currentQuestion } = this.props;
    const { question, inputValue } = this.state;

    // Only dispatch ANSWER_QUESTION if the input value changed
    if (inputValue !== this.getSavedValue(question)) {
      dispatch(addAnswer({ question: question.value, answer: inputValue }));
    }

    // Check for last question to set next view
    if (!question.last) {
      dispatch(setQuestion(currentQuestion + 1));
    } else {
      dispatch(answerLastQuestion());
    }
  }

  render() {
    const { inputValue, question } = this.state;

    return (
      <>
        <AnimatedText text={question.text} />
        <Form id={question.value} onSubmit={this.handleSubmit}>
          <FormInput question={question} handleChange={this.handleChange} value={inputValue} />
        </Form>
        <ButtonContainer>
          <Button onClick={this.handleBack}> &lt; Anterior </Button>
          <Button form={question.value} type="submit" disabled={inputValue === ''}> Siguiente &gt; </Button>
        </ButtonContainer>
      </>
    );
  }
}

Question.propTypes = {
  dispatch: PropTypes.func.isRequired,
  currentQuestion: PropTypes.number.isRequired,
  answers: PropTypes.shape({}).isRequired
};

const mapStateToProps = state => ({
  currentQuestion: state.view.currentQuestion,
  answers: state.answers
});

export default connect(mapStateToProps)(Question);
