import React from 'react';

import errorImage from './error.svg';

import Title from '../../components/Title'
import Text from '../../components/Text'
import Image from '../../components/Image'

const Error = () => (
  <div>
    <Title>
      ¡Oh no!
    </Title>
    <Text>Parece que tu celular no es compatible con el Sincroverso</Text>
    <Image src={errorImage} />
    <Text small>
      No pierdas las esperanzas, todavía podes pedir uno
      prestado a alguien que esté cerca y experimentarlo
    </Text>
  </div>
);

export default Error;
