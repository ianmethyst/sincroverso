import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import SETTINGS from '../../settings';

import { setView, catchError, sendAnswers } from '../../actions/creators';

import views from '../../views';
import errors from '../../data/errorTypes';

import Title from '../../components/Title';
import Text from '../../components/Text';
import Button from '../../components/Button';
import ButtonContainer from '../../components/ButtonContainer';

const text = `
  Este es el final de esta parte de la historia. Cuando pulses
  en ENTRAR, deberás seguir las instrucciones para adentrarte
  en el Sincroverso.
`;

const smallText = `
  (Si no estás conforme con alguna de tus respuestas,
  podes volver atrás y cambiarla).
`;

const Confirm = ({ allAnswered, dispatch }) => {
  if (!allAnswered) {
    dispatch(catchError(errors.MISSING_ANSWERS));
  }

  const enterAction = SETTINGS.online
    ? () => dispatch(sendAnswers())
    : () => dispatch(setView(views.ENTER));

  return (
    <>
    <Title>
      ¡Excelente!
    </Title>
    <Text>
      {text}
    </Text>
    <Text small>
      {smallText}
    </Text>
    <ButtonContainer>
      <Button onClick={() => dispatch(setView(views.QUESTION))}> &lt; Volver </Button>
      <Button onClick={() => enterAction()}> Entrar &gt; </Button>
    </ButtonContainer>
    </>
  );
};

Confirm.propTypes = {
  dispatch: PropTypes.func.isRequired,
  allAnswered: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  allAnswered: state.flags.allAnswered
});

export default connect(mapStateToProps)(Confirm);
