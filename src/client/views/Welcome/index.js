import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import interviewImage from './interview.svg';

import { setView } from '../../actions/creators';

import views from '../../views';

import Title from '../../components/Title';
import Text from '../../components/Text';
import Image from '../../components/Image';
import Button from '../../components/Button';
import ButtonContainer from '../../components/ButtonContainer';

const Welcome = ({
  answers,
  allAnswered,
  currentQuestion,
  dispatch
}) => {
  const answersGiven = Object.keys(answers).length;

  const buttonText = answersGiven === 0 ? '¡Empecemos! >' : 'Continuar >';
  const view = (allAnswered && currentQuestion !== 0) ? views.CONFIRM : views.QUESTION;

  return (
    <>
      <Title>
       ¡Te damos la bienvenida!
      </Title>
      <Text>
        {'Antes de poder acceder al '}
        <strong>Sincroverso</strong>
        {' necesitamos que respondas una serie de preguntas'}
      </Text>
      <Image src={interviewImage} />
      <Text small>
        {'¡No hay apuro así que tomate el tiempo que necesites! Tu honestidad es clave para disfrutar al máximo la experiencia.'}
      </Text>
      <ButtonContainer>
        <Button big onClick={() => dispatch(setView(view))}>
          { buttonText }
        </Button>
      </ButtonContainer>
    </>
  );
};

Welcome.propTypes = {
  dispatch: PropTypes.func.isRequired,
  answers: PropTypes.shape({}).isRequired,
  allAnswered: PropTypes.bool.isRequired,
  currentQuestion: PropTypes.number.isRequired
};

const mapStateToProps = state => ({
  currentQuestion: state.view.currentQuestion,
  answers: state.answers,
  allAnswered: state.flags.allAnswered
});

export default connect(mapStateToProps)(Welcome);
