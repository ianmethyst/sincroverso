import React from 'react';
import styled from 'styled-components';

import Title from '../../components/Title'
import Text from '../../components/Text'

const Position = styled.div`
  display: flex;
  flex-directon: column;
  justify-content: center;
  align-items: center;

  font-size: 1.5em;

  width: 5em;
  height: 5em;
  margin: 0 auto;

  border: 0.15em solid white;
  border-radius: 50%;

  p {
    margin: 0;
    padding: 0;
    font-size: 3.5em;
    font-family: 'Helvetica', sans-serif;
  }
`;

const Queue = () => (
  <div>
    <Title>
      Estas en la cola de espera
    </Title>
    <Text>Cuando se desocupe un lugar en el recinto vas a poder entrar al sincroverso</Text>
    <Text small>Tu posición en la cola:</Text>
    <Position><p>2</p></Position>
    <Text>Tiempo de espera estimado: 5 minutos</Text>
  </div>
);

export default Queue;
