import React from 'react';

import { BreedingRhombusSpinner } from 'react-epic-spinners';

const Loading = () => (
  <BreedingRhombusSpinner
    color="white"
    style={{ alignSelf: 'center' }}
  />
);

export default Loading;
