import WebVRPolyfill from 'webvr-polyfill';

const polyfill = () => (
  new WebVRPolyfill({
    BUFFER_SCALE: 0.5,
    CARDBOARD_UI_DISABLED: true,
    ROTATE_INSTRUCTIONS_DISABLED: true,
  })
);

export default polyfill;
