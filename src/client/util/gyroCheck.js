
const gyroAvaiable = new Event('gyroavaiable');

export const gyroCheck = (event) => {
  if (event.rotationRate.alpha || event.rotationRate.beta || event.rotationRate.gamma) {
    console.log(event);
    window.dispatchEvent(gyroAvaiable);
  }
};

export const checkSuccess = (timeout, dispatch, action) => {
  window.removeEventListener('devicemotion', gyroCheck, false);
  window.removeEventListener('gyroavailable', this, false);
  clearTimeout(timeout);
  dispatch({ type: action });
};
