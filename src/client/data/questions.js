import inputTypes from './inputTypes';

import {
  parameters,
  faunaTypes,
  biomes,
  terrains,
  seasons,
  weathers
} from './worldParameters';

const questions = [
  {
    id: 0,
    value: parameters.NAME,
    text: 'Antes de comenzar con la historia ¿Podrías decirnos como te llaman?',
    input: inputTypes.TEXT,
  },
  {
    id: 1,
    value: parameters.ATMOSPHERE,
    text: 'Al despertarte, recordás que tenés planeado comprar pintura para las paredes. ¿Qué color elegirás?',
    input: inputTypes.COLOR_PICKER,
  },
  {
    id: 2,
    value: parameters.WEATHER,
    text: 'Antes de salir, volteas y contemplas tu escritorio. ¿Qué tan ordenado está?',
    input: inputTypes.RADIO,
    options: [
      {
        option: 'Todo está en su lugar',
        value: weathers.DRY
      },
      {
        option: 'Todo está desordenado',
        value: weathers.WET
      }
    ],
  },
  {
    id: 3,
    value: parameters.TERRAIN,
    text: 'En el buzón descubrís una invitación para el torneo deportivo al que querías ir. ¿Cuál deporte sería?',
    input: inputTypes.RADIO,
    options: [
      {
        option: 'Esquí',
        value: terrains.MOUNTAIN,
      },
      {
        option: 'Surf',
        value: terrains.ISLE,
      },
      {
        option: 'Fútbol',
        value: terrains.PLAIN
      }
    ],
  },
  {
    id: 4,
    value: parameters.DAYTIME,
    text: 'Saliendo de casa, mirás el reloj y descubrís que se quedó trabado en la mejor hora del día. ¿Cuál sería?',
    input: inputTypes.TIME,
  },
  {
    id: 5,
    value: parameters.BIOME,
    text: 'De camino a la pinturería, ves un bazar con objetos decorativos. ¿Con cuáles decorarías tu casa?',
    input: inputTypes.RADIO,
    options: [
      {
        option: 'Macetas',
        value: biomes.GRASSY,
      },
      {
        option: 'Esculturas',
        value: biomes.ROCKY,
      },
      {
        option: 'No decoraría mi casa',
        value: biomes.DESERTIC
      }
    ],
  },
  {
    id: 6,
    value: parameters.FAUNA,
    text: 'Al volver a casa, tu pareja te critica por el color elegido. ¿Cuál es tu reacción?',
    input: inputTypes.RADIO,
    options: [
      {
        option: 'Terminás la relación',
        value: faunaTypes.FREE
      },
      {
        option: 'Le abrazas para convencerle',
        value: faunaTypes.DOMESTIC
      },
      {
        option: 'Discutís y le gritás',
        value: faunaTypes.CARNIVORE
      },
      {
        option: 'Evitás la discusión',
        value: faunaTypes.HERVIBORE
      }
    ],
  },
  {
    id: 7,
    value: parameters.FLORA_COLOR,
    text: 'Pasas por tu estudio y decidís finalmente el color para el logotipo de tu campaña de protección del medioambiente. ¿Cuál sería?',
    input: inputTypes.COLOR_PICKER,
  },
  {
    id: 8,
    value: parameters.SEASON,
    text: 'Al acostarte, recordás que tenes que despertarte temprano. ¿decidís configurar una o varias alarmas?',
    input: inputTypes.RADIO,
    options: [
      {
        option: 'Una sola alarma',
        value: seasons.SUMMER
      },
      {
        option: 'Varias alarmas',
        value: seasons.WINTER
      }
    ],
    last: true
  }
];

export default questions;
