export const animalPositions = {
  PLAIN: {
    position: [0, 0.0, 0],
    rotation: [0.1, 0, 0.3]
  },
  MOUNTAIN: {
    position: [0, 0.25, 0],
    rotation: [0.3, -0.2, 0.1]
  },
  ISLE: {
    position: [0, 0.5, 0],
    rotation: [0.3, 0, 0]
  }
};

export const floraPositions = {
  PLAIN: [
    [0.1, 0, -0.3],
    [-0.3, 0, -0.6],
    [0.8, 0, 0],
    [-0.1, 0, 0.7],
    [-0.6, 0, 0.2],
  ],
  MOUNTAIN: [
    [0.1, 0, -0.3],
    [-0.4, 0, 0.2],
    [0, 0, 0.7]
  ],
  ISLE: [
    [0.4, 0, -0.2],
    [0.1, 0, 0.4],
    [0.3, 0, 0.2],
  ]
};
