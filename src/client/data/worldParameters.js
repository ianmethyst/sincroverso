export const parameters = {
  NAME: 'NAME',
  TERRAIN: 'TERRAIN',
  BIOME: 'BIOME',
  FAUNA: 'FAUNA',
  /* FLORA: 'FLORA', flora is determied by biome */
  FLORA_COLOR: 'FLORA_COLOR',
  SEASON: 'SEASON',
  ATMOSPHERE: 'ATMOSPHERE',
  DAYTIME: 'DAYTIME',
  WEATHER: 'WEATHER',
};

export const biomes = {
  GRASSY: 'GRASSY',
  DESERTIC: 'DESERTIC',
  ROCKY: 'ROCKY'
};

export const terrains = {
  PLAIN: 'PLAIN',
  MOUNTAIN: 'MOUNTAIN',
  ISLE: 'ISLE',
};

export const seasons = {
  WINTER: 'WINTER',
  SUMMER: 'SUMMER'
};

export const weathers = {
  DRY: 'DRY',
  WET: 'WET'
};


export const flora = {
  STONE: 'STONE',
  BUSH: 'BUSH',
  PALMTREE: 'PALMTREE',
  CACTUS: 'CACTUS',
  TREE: 'TREE'
};

export const biomeFlora = {
  GRASSY: [
    flora.TREE,
    flora.BUSH
  ],
  ROCKY: [
    flora.STONE,
    flora.BUSH
  ],
  DESERTIC: [
    flora.CACTUS,
    flora.PALMTREE
  ],
};

export const animals = {
  PENGUIN: { key: 'PENGUIN', scale: 0.9 },
  EAGLE: { key: 'EAGLE', scale: 0.85 },
  GOOSE: { key: 'GOOSE', scale: 0.90 },
  OSTRICH: { key: 'OSTRICH', scale: 1.5 },
  LION: { key: 'LION', scale: 2 },
  BEAR: { key: 'BEAR', scale: 1.6 },
  FOX: { key: 'FOX', scale: 1.2 },
  HORSE: { key: 'HORSE', scale: 1.8 },
  DEER: { key: 'DEER', scale: 1.6 },
  GIRAFFE: { key: 'GIRAFFE', scale: 2.8 },
  ELEPHANT: { key: 'ELEPHANT', scale: 2.5 },
  HIPPOPOTAMUS: { key: 'HIPPOPOTAMUS', scale: 2.5 },
  DOG: { key: 'DOG', scale: 1 },
  CAT: { key: 'CAT', scale: 0.9 },
  TURTLE: { key: 'TURTLE', scale: 0.6 }
};

export const faunaTypes = {
  FREE: 'FREE',
  DOMESTIC: 'DOMESTIC',
  CARNIVORE: 'CARNIVORE',
  HERVIBORE: 'HERVIBORE'
};

export const fauna = {
  FREE: [
    animals.PENGUIN,
    animals.EAGLE,
    animals.GOOSE,
    animals.OSTRICH
  ],
  CARNIVORE: [
    animals.LION,
    animals.BEAR,
    animals.FOX,
  ],
  HERVIBORE: [
    animals.HORSE,
    animals.DEER,
    animals.GIRAFFE,
    animals.ELEPHANT,
    animals.HIPPOPOTAMUS
  ],
  DOMESTIC: [
    animals.DOG,
    animals.CAT,
    animals.TURTLE
  ]
};

export const materialNames = {
  GRASS: 'grass',
  GROUND: 'earth',
  WATER: 'water',
  LEAF: 'leaf',
  SNOW: 'snow',
  ROCK: 'rock',
  WOOD: 'wood'
};

export const materialColors = {
  ROCK: 0xBCBCBC,
  GROUND: 0xAA6C3D,
  SAND: 0xD5C46B,
  SNOW: 0xF4F4F4
};
