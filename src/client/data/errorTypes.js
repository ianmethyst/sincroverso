const errors = {
  UNDEFINED_VIEW: {
    type: 'UNDEFINED_VIEW',
    description: 'View not defined'
  },
  INCOMPATIBLE: {
    type: 'INCOMPATIBLE',
    description: 'Gyroscope required and not found'
  },
  UNDEFINED_INPUT: {
    type: 'UNDEFINED_INPUT',
    description: 'Tried to create <input> element with wrong inputType'
  },
  MISSING_ANSWERS: {
    type: 'MISSING_ANSWERS',
    description: 'Got to Confirm view but allAnswered flag was false'
  },
  VR_NOTFOUND: {
    type: 'VR_NOTFOUND',
    description: 'VR display required but not found. Use polyfill or Chrome WebVR API Emulation extension'
  },
  /*
  MODEL_LOAD_FAILURE: {
    type: 'MODEL_LOAD_FAILURE',
    description: '3D models load failure'
  }
  */
  THREE_ERROR: {
    type: 'THREE_ERROR',
    description: 'There was an error within the <Three /> component. Check log'
  }
};

export default errors;
