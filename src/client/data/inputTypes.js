const inputTypes = {
  TEXT: 'TEXT',
  RADIO: 'RADIO',
  COLOR_PICKER: 'COLOR_PICKER',
  TIME: 'TIME'
};

export default inputTypes;
