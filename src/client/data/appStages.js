const stages = {
  INIT: 'INIT',
  STORY: 'STORY',
  IMMERSION: 'IMMERSION',
  END: 'END'
}

export default stages;
