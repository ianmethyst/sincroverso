import { terrains, animals, flora } from './worldParameters';

const resources = [
  { key: terrains.PLAIN, resource: 'model/world/plain.glb' },
  { key: terrains.ISLE, resource: 'model/world/isle.glb' },
  { key: terrains.MOUNTAIN, resource: 'model/world/mountain.glb' },

  { key: animals.HORSE.key, resource: 'model/animal/horse.glb' },
  { key: animals.PENGUIN.key, resource: 'model/animal/penguin.glb' },
  { key: animals.OSTRICH.key, resource: 'model/animal/ostrich.glb' },
  { key: animals.DOG.key, resource: 'model/animal/dog.glb' },
  { key: animals.CAT.key, resource: 'model/animal/cat.glb' },
  { key: animals.ELEPHANT.key, resource: 'model/animal/elephant.glb' },
  { key: animals.GOOSE.key, resource: 'model/animal/goose.glb' },
  { key: animals.FOX.key, resource: 'model/animal/fox.glb' },
  { key: animals.HIPPOPOTAMUS.key, resource: 'model/animal/hippopotamus.glb' },
  { key: animals.BEAR.key, resource: 'model/animal/bear.glb' },
  { key: animals.DEER.key, resource: 'model/animal/deer.glb' },
  { key: animals.LION.key, resource: 'model/animal/lion.glb' },
  { key: animals.GIRAFFE.key, resource: 'model/animal/giraffe.glb' },
  { key: animals.TURTLE.key, resource: 'model/animal/turtle.glb' },
  { key: animals.EAGLE.key, resource: 'model/animal/eagle.glb' },

  { key: flora.STONE, resource: 'model/flora/stone.glb' },
  { key: flora.BUSH, resource: 'model/flora/bush.glb' },
  { key: flora.PALMTREE, resource: 'model/flora/palmtree.glb' },
  { key: flora.CACTUS, resource: 'model/flora/cactus.glb' },
  { key: flora.TREE, resource: 'model/flora/tree.glb' },
];

export default resources;
