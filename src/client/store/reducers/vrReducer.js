import {
  MODELS_LOAD_REQUEST,
  MODELS_LOAD_FAILURE,
  MODELS_LOAD_SUCCESS,
  SET_RENDERER,
  SET_VRDISPLAY,
  SET_PRESENT
} from '../../actions/types';

const INITIAL_STATE = {
  renderer: null,
  vrDisplay: null,
  presenting: false,
  models: {
    loading: false,
    loaded: false,
    failure: false
  }
};

const vrReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_RENDERER:
      return { ...state, renderer: action.renderer };
    case SET_VRDISPLAY:
      return { ...state, vrDisplay: action.display };
    case SET_PRESENT:
      return { ...state, presenting: action.presenting };
    case MODELS_LOAD_REQUEST:
      return { ...state, models: { ...state.models, loading: true } };
    case MODELS_LOAD_FAILURE:
      return {
        ...state,
        models: {
          loading: false,
          loaded: true,
          failure: true
        }
      };
    case MODELS_LOAD_SUCCESS:
      return {
        ...state,
        models: {
          loading: false,
          loaded: true,
          failure: false
        }
      };
    default:
      return state;
  }
};

export default vrReducer;
