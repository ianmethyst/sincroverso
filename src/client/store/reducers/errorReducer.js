import { CATCH_ERROR } from '../../actions/types';

const errorReducer = (state = {}, action) => {
  switch (action.type) {
    case CATCH_ERROR:
      return action.error;
    default:
      return state;
  }
};

export default errorReducer;
