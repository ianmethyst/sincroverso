import { RESET_ANSWERS, ANSWER_QUESTION } from '../../actions/types';

function answerReducer(state = {}, action) {
  switch (action.type) {
    case ANSWER_QUESTION: {
      return Object.assign({}, state, { [action.answer.question]: action.answer.answer });
    }

    case RESET_ANSWERS: {
      return {};
    }

    default:
      return state;
  }
}

export default answerReducer;
