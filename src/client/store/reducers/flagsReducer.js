import {
  COMPATIBILITY_CHECK_REQUEST,
  COMPATIBILITY_CHECK_SUCCESS,
  COMPATIBILITY_CHECK_FAILURE,
  BEGIN_IMMERSION,
  ANSWER_LAST,
  SET_STAGE
} from '../../actions/types';

import stages from '../../data/appStages';

const INITIAL_STATE = {
  compatible: false,
  isChecking: false,
  compatibilityChecked: false,
  isLoading: false,
  immersionBegan: false,
  allAnswered: false,
  appStage: stages.INIT
};

const flagsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case COMPATIBILITY_CHECK_REQUEST: {
      return {
        ...state,
        isChecking: true
      };
    }
    case COMPATIBILITY_CHECK_SUCCESS: {
      return {
        ...state,
        compatible: true,
        isChecking: false,
        compatibilityChecked: true
      };
    }
    case COMPATIBILITY_CHECK_FAILURE: {
      return {
        ...state,
        compatible: false,
        isChecking: false,
        compatibilityChecked: true
      };
    }
    case BEGIN_IMMERSION: {
      return {
        ...state,
        immersionBegan: false
      };
    }
    case ANSWER_LAST: {
      return {
        ...state,
        allAnswered: true
      };
    }
    case SET_STAGE:
      return {
        ...state,
        appStage: action.appStage
      }
    default:
      return state;
  }
};

export default flagsReducer;
