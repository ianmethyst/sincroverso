import {
  CHANGE_VIEW,
  CHANGE_CURRENT_QUESTION
} from '../../actions/types';
import views from '../../views';


const viewReducer = (state = { view: views.SPLASH, currentQuestion: 0 }, action) => {
  switch (action.type) {
    case CHANGE_VIEW:
      return { ...state, view: action.view };
    case CHANGE_CURRENT_QUESTION:
      return { ...state, currentQuestion: action.currentQuestion };
    default:
      return state;
  }
};

export default viewReducer;
