import { combineReducers } from 'redux';

import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import flagsReducer from './flagsReducer';
import errorReducer from './errorReducer';
import viewReducer from './viewReducer';
import answerReducer from './answerReducer';
import vrReducer from './vrReducer';

const rootPersistConfig = {
  key: 'root',
  storage,
  blacklist: ['flags', 'view', 'errors', 'vr']
};

const flagsPersistConfig = {
  key: 'flags',
  storage,
  whitelist: ['compatible', 'comaptibilityChecked']
};

const rootReducer = combineReducers({
  flags: persistReducer(flagsPersistConfig, flagsReducer),
  view: viewReducer,
  errors: errorReducer,
  answers: answerReducer,
  vr: vrReducer
});

export default persistReducer(rootPersistConfig, rootReducer);
