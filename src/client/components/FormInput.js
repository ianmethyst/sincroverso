import React from 'react';
import PropTypes from 'prop-types';

import { SliderPicker } from 'react-color';

import { catchError } from '../actions/creators';

import inputTypes from '../data/inputTypes';
import errors from '../data/errorTypes';

const FormInput = ({
  dispatch,
  question,
  handleChange,
  value
}) => {
  switch (question.input) {
    case inputTypes.TEXT: {
      return (
        <input
          type="text"
          value={value}
          onChange={handleChange}
          placeholder="Escribilo acá"
          minLength="3"
          maxLength="20"
          required
        />
      );
    }

    case inputTypes.RADIO: {
      const options = question.options.map(option => (
        <div key={option.value}>
          <input
            id={option.value}
            checked={value === option.value}
            onChange={handleChange}
            name="radio"
            type="radio"
            value={option.value}
          />
          <label htmlFor={option.value}>
            {option.option}
          </label>
        </div>
      ));
      return options;
    }

    case inputTypes.TIME: {
      return (
        <input
          type="time"
          name="time"
          onChange={handleChange}
          value={value}
        />
      );
    }

    case inputTypes.COLOR_PICKER: {
      return (
        <div>
          <SliderPicker
            color={value === '' ? '#00ffff' : value}
            onChangeComplete={handleChange}
          />
        </div>
      );
    }

    default:
      return dispatch(catchError(errors.INPUT_UNDEFINED));
  }
};

FormInput.propTypes = {
  handleChange: PropTypes.func.isRequired,
  /*  question: PropTypes.object.shape({ }).isRequired, */
  value: PropTypes.string.isRequired
};

export default FormInput;
