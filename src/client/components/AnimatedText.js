import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import SplitText from 'react-pose-text';

const charPoses = {
  enter: { opacity: 1, y: '0%', delay: ({ charIndex }) => charIndex * 50 },
  exit: { opacity: 0, y: '100%' }
};

const SplitWrapper = styled.div`
  display: block;
  margin: 1.5em auto 0 auto;
  text-align: center;
`;

const Split = styled(SplitText)`
  text-align: center;
  font-size: 1.15em;
  line-height: 1.3em;
`;

const AnimatedText = ({ text }) => (
  <SplitWrapper>
    <Split charPoses={charPoses} initialPose="exit" pose="enter">{text}</Split>
  </SplitWrapper>
);

AnimatedText.propTypes = {
  text: PropTypes.string.isRequired
};

export default AnimatedText;
