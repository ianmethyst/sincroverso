import styled from 'styled-components';

const Button = styled.button`
  display: block;

  margin: 1em auto;
  padding: 0.5em 0.25em;
  border: 0.15em solid white;

  width: ${props => (props.big ? 95 : 45)}%;
  height: 100%;

  color: white;
  background: none;

  font-size: 1.2em;
  font-weight: bold;

  cursor: pointer;

  &:disabled { 
    cursor: default;
    opacity: 0.25;
  }
`;

export default Button;
