import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }

  #app {
    height: 100%
  }

  html, body {
    overflow: hidden;
    margin: 0;
    padding: 0;
    width: 100vw;
    /*height: 100vh;*/
    height: ${props => (props.height * 100)}px;

    background: black;
    color: white;
    font-family: 'Raleway', sans-serif;
    font-feature-settings: "lnum"; 
    overflow: hidden;
  }
`;

export default GlobalStyle;
