import styled from 'styled-components';

const ViewContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: flex-start;
  height: 100%;
`;

export default ViewContainer;
