import styled from 'styled-components';

const Text = styled.p`
  margin: 1em 0 0 0;
  padding: 0 0.25em;

  line-height: 1.3em;

  ${props => (props.small ? 'color: rgba(255, 255, 255, 0.72);' : '')}

  text-align: center;
  font-size: ${props => (props.small ? 1 : 1.15)}em;
`;

export default Text;
