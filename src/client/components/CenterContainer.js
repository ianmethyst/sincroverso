import styled from 'styled-components';

const CenterContainer = styled.div`
  display: flex;

  width: 100%;
  height: 100%;

  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default CenterContainer;
