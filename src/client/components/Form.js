import styled from 'styled-components';

const Form = styled.form`
  align-self: center;

  display: flex;

  flex-direction: column;
  justify-content: center;
  align-items: flex-start;

  margin: 0 0.5em;
  padding: 0 0.5em;

  width: 100%;
  height: 55%;

  overflow-y: auto;
  overflow-x: hidden;

  input[type="text"] {
    display: block;

    margin: 1em auto;
    padding: 0.5em 0.75em;
    border: 0.15em solid white;

    width: 100%;

    background: none;
    color: white;

    font-size: 1.25em;
    font-weight: bold;

    &::placeholder {
      color: rgba(255, 255, 255, 0.32);
    }
  }

  input[type="time"] {
    display: block;
    width: 100%;
    text-align:center;
    margin: 1em auto;
    background: none;
    font-size: 1.25em;
    color: white;
    padding: 0.5em 0.75em;
    font-weight: bold;
    border: 0.15em solid white;
  }

  // Label container
  div {
    width: 100%;
  }

  label {
    display: block;
    width: 100%;
    position: relative;

    margin: 0.5em 0;
    padding: 0.5em 0.75em;
    border: 0.15em solid white;
    opacity: 0.8;

    text-align: center;
    font-size: 1.2em;
    font-weight: bold;
    color: white;
  }

  input[type="radio"] {
    display: none;
  }

  input[type="radio"]:checked + label {
    opacity: 1;
    background: rgba(68, 255, 68, 0.2);
  }
`;

export default Form;
