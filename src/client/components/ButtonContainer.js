import styled from 'styled-components';

const ButtonContainer = styled.div`
  align-self: flex-end;

  display: flex;
  flex-direction: row;

  margin-bottom: 7vh;

  width: 100%;
  height: 10%;
`;

export default ButtonContainer;
