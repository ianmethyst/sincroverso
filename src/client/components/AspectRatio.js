import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { PoseGroup } from 'react-pose';

const Wrapper = styled(PoseGroup)`
  position: relative;
  width: ${props => (props.percentage)}vh;
  height: 100%;

  margin: ${props => (props.centered ? '0 auto' : 0)};
  padding: 0;

  overflow: hidden;

  font-size: 3vh;

  background: rgba(0, 0, 0, 0.5);
`;

const Container = ({ ratio, centered, show, children }) => {
  const [w, h] = ratio.split(':');
  const percent = (w / h) * 100;

  if (!show) {
    return null;
  }

  return (
    <Wrapper percentage={percent} centered={centered}>
      {children}
    </Wrapper>
  );
};

Container.propTypes = {
  ratio: PropTypes.string,
  centered: PropTypes.bool,
  show: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired
};

Container.defaultProps = {
  centered: true,
  ratio: '9:16',
};

export default Container;
