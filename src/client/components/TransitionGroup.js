import styled from 'styled-components';
import posed from 'react-pose';

const Pose = posed.div({
  enter: { opacity: 1 },
  exit: { opacity: 0 }
});

// before: PoseWrapper
const TransitionGroup = styled(Pose)`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  align-items; center;
  justify-content: center;
  height: 100%;
`;

export default TransitionGroup;
