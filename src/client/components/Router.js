import React from 'react';

import {
  BrowserRouter,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';

import App from '../App';
import Sandbox from '../views/Sandbox';

const Redir = () => (<Redirect to="/" />);

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={App} />
      <Route exact path="/___sandbox" component={Sandbox} />
      <Route component={Redir} />
    </Switch>
  </BrowserRouter>
);

export default Router;
