import styled from 'styled-components';

const Title = styled.h2`
  margin: 1em 0 0 0;

  text-align: center;
  font-size: ${props => (props.splash ? 1.8 : 1.5)}em;
`;

export default Title;
