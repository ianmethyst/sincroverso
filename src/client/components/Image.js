import styled from 'styled-components';

const Image = styled.img`
  display: block;
  height: 25%;
  max-width: 80%;
  margin: 2em auto;
  padding: 0;
`;

export default Image;
