import React from 'react';
import ReactDOM from 'react-dom';

import Router from './components/Router';
import StoreProvider from './store/Provider';

import vrPolyfill from './util/vrPolyfill';

import 'normalize.css';
import 'typeface-raleway';

vrPolyfill();

ReactDOM.render(
  <StoreProvider>
    <Router />
  </StoreProvider>, document.getElementById('app')
);
